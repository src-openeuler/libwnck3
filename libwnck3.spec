Name:           libwnck3
Version:        40.1
Release:        1
Summary:        Window Navigator Construction Kit
URL:            http://download.gnome.org/sources/libwnck/
Source0:        http://download.gnome.org/sources/libwnck/40/libwnck-%{version}.tar.xz
License:        LGPLv2+
BuildRequires:  glib2-devel gtk3-devel gtk-doc meson pango-devel libxslt
BuildRequires:  startup-notification-devel gobject-introspection-devel libXres-devel gettext
Requires:       startup-notification

%description
libwnck is used to implement pagers, tasklists, and other such things.
It allows applications to monitor information about open windows,
workspaces, their names/icons, and so forth.

%package        devel
Summary:        Libraries and headers for {%name}
Requires:       %{name} = %{version}-%{release}

%description    devel
The %{name}-devel package contains libraries and header files for %{name}.

%package_help

%prep
%autosetup -n libwnck-%{version} -p1

%build
%meson -Dgtk_doc=true
%meson_build

%install
%meson_install

%ldconfig_scriptlets

%files
%defattr(-,root,root)
%doc AUTHORS
%license COPYING
%{_libdir}/*-3.so*
%{_bindir}/wnck*
%{_datadir}/locale/*
%{_libdir}/girepository-1.0/Wnck-3.0.typelib

%files          devel
%defattr(-,root,root)
%{_libdir}/pkgconfig/*
%{_includedir}/libwnck-3.0/*
%{_datadir}/gir-1.0/Wnck-3.0.gir

%files          help
%defattr(-,root,root)
%doc README NEWS
%doc %{_datadir}/gtk-doc/*

%changelog
* Mon Sep 12 2022 tianlijing <tianlijing@kylinos.cn> - 40.1-1
- update to 40.1

* Wed Jan 27 2021 hanhui <hanhui15@huawei.com> - 3.36.0-1
- Type: enhancement
- ID:   NA
- SUG:  NA
- DESC: update to 3.36.0

* Tue Sep 08 2020 zhanzhimin <zhanzhimin@huawei.com> - 3.31.4-4
- update source0

* Fri Jan 17 2020 openEuler Buildteam <buildteam@openeuler.org> - 3.31.4-3
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:change the install dir in mesonbuild 

* Thu Dec 26 2019 openEuler Buildteam <buildteam@openeuler.org> - 3.31.4-2
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:fix build fail

* Thu Aug 29 2019 openEuler Buildteam <buildteam@openeuler.org> - 3.31.4-1
- Package init
